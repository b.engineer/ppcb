EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Shield_cyc1000_RaspberryPi4_HegerBoard2_1"
Date "2020-04-17"
Rev "1.0"
Comp "markju GmbH"
Comment1 "Designed by Adam Hochstuhl"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+5V #PWR01
U 1 1 580C1B61
P 2950 750
F 0 "#PWR01" H 2950 600 50  0001 C CNN
F 1 "+5V" H 2950 890 50  0000 C CNN
F 2 "" H 2950 750 50  0000 C CNN
F 3 "" H 2950 750 50  0000 C CNN
	1    2950 750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 750  2950 900 
Wire Wire Line
	2950 900  2750 900 
Wire Wire Line
	2950 1000 2750 1000
Connection ~ 2950 900 
$Comp
L power:GND #PWR02
U 1 1 580C1D11
P 2850 2950
F 0 "#PWR02" H 2850 2700 50  0001 C CNN
F 1 "GND" H 2850 2800 50  0000 C CNN
F 2 "" H 2850 2950 50  0000 C CNN
F 3 "" H 2850 2950 50  0000 C CNN
	1    2850 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 1100 2850 1500
Wire Wire Line
	2850 2500 2750 2500
Wire Wire Line
	2850 2300 2750 2300
Connection ~ 2850 2500
Wire Wire Line
	2850 1800 2750 1800
Connection ~ 2850 2300
Wire Wire Line
	2850 1500 2750 1500
Connection ~ 2850 1800
$Comp
L power:GND #PWR03
U 1 1 580C1E01
P 2150 2950
F 0 "#PWR03" H 2150 2700 50  0001 C CNN
F 1 "GND" H 2150 2800 50  0000 C CNN
F 2 "" H 2150 2950 50  0000 C CNN
F 3 "" H 2150 2950 50  0000 C CNN
	1    2150 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 2800 2250 2800
Wire Wire Line
	2150 1300 2150 2100
Wire Wire Line
	2150 2100 2250 2100
Connection ~ 2150 2800
Wire Wire Line
	2150 1300 2250 1300
Connection ~ 2150 2100
Wire Wire Line
	2250 1000 1100 1000
Wire Wire Line
	1100 1100 2250 1100
Wire Wire Line
	1100 1200 2250 1200
Wire Wire Line
	2250 1400 1100 1400
Wire Wire Line
	1100 1500 2250 1500
Wire Wire Line
	1100 1600 2250 1600
Wire Wire Line
	2250 1800 1100 1800
Wire Wire Line
	1100 1900 2250 1900
Wire Wire Line
	1100 2000 2250 2000
Wire Wire Line
	2250 2200 1100 2200
Wire Wire Line
	1100 2300 2250 2300
Wire Wire Line
	1100 2400 2250 2400
Wire Wire Line
	2250 2500 1100 2500
Wire Wire Line
	1100 2600 2250 2600
Wire Wire Line
	1100 2700 2250 2700
Wire Wire Line
	2750 2600 3800 2600
Wire Wire Line
	2750 2700 3800 2700
Wire Wire Line
	2750 2100 3800 2100
Wire Wire Line
	2750 2200 3800 2200
Wire Wire Line
	2750 1900 3800 1900
Wire Wire Line
	2750 2000 3800 2000
Wire Wire Line
	2750 1600 3800 1600
Wire Wire Line
	2750 1700 3800 1700
Wire Wire Line
	2750 1300 3800 1300
Wire Wire Line
	2750 1400 3800 1400
Wire Wire Line
	2750 1200 3800 1200
Wire Wire Line
	2750 2400 3800 2400
Text Label 1100 2200 0    50   ~ 0
ID_SD
Text Label 1100 2300 0    50   ~ 0
RP_GPIO5
Text Label 1100 2400 0    50   ~ 0
RP_GPIO6
Text Label 1100 2500 0    50   ~ 0
RP_GPIO13(PWM1)
Text Label 1100 2600 0    50   ~ 0
RP_GPIO19(SPI1_MISO)
Text Label 1100 2700 0    50   ~ 0
RP_GPIO26
Text Label 3800 2700 2    50   ~ 0
RP_GPIO20(SPI1_MOSI)
Text Label 3800 2600 2    50   ~ 0
RP_GPIO16
Text Label 3800 2400 2    50   ~ 0
RP_GPIO12(PWM0)
Text Label 3800 2200 2    50   ~ 0
ID_SC
Text Label 3800 2100 2    50   ~ 0
RP_GPIO7(SPI1_CE_N)
Text Label 3800 2000 2    50   ~ 0
RP_GPIO8(SPI0_CE_N)
Text Label 3800 1900 2    50   ~ 0
RP_GPIO25(GEN6)
Text Label 3800 1700 2    50   ~ 0
RP_GPIO24(GEN5)
Text Label 3800 1600 2    50   ~ 0
RP_GPIO23(GEN4)
Text Label 3800 1400 2    50   ~ 0
RP_GPIO18(GEN1)(PWM0)
Text Label 3800 1300 2    50   ~ 0
RP_GPIO15(RXD0)
Text Label 3800 1200 2    50   ~ 0
RP_GPIO14(TXD0)
Wire Wire Line
	2850 1100 2750 1100
Connection ~ 2850 1500
Text Notes 600  5550 0    50   ~ 0
Mounting Holes
$Comp
L Connector_Generic:Conn_02x20_Odd_Even P1
U 1 1 59AD464A
P 2450 1800
F 0 "P1" H 2500 2917 50  0000 C CNN
F 1 "Conn_02x20_Odd_Even" H 2500 2826 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x20_P2.54mm_Vertical" H -2400 850 50  0001 C CNN
F 3 "" H -2400 850 50  0001 C CNN
	1    2450 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2750 2800 3800 2800
Text Label 3800 2800 2    50   ~ 0
RP_GPIO21(SPI1_SCK)
Wire Wire Line
	2950 900  2950 1000
Wire Wire Line
	2850 2500 2850 2950
Wire Wire Line
	2850 2300 2850 2500
Wire Wire Line
	2850 1800 2850 2300
Wire Wire Line
	2150 2800 2150 2950
Wire Wire Line
	2150 2100 2150 2800
Wire Wire Line
	2850 1500 2850 1800
Text Label 1100 2000 0    50   ~ 0
RP_GPIO11(SPI0_SCK)
Text Label 1100 1900 0    50   ~ 0
RP_GPIO9(SPI0_MISO)
Text Label 1100 1800 0    50   ~ 0
RP_GPIO10(SPI0_MOSI)
Text Label 1100 1600 0    50   ~ 0
RP_GPIO22(GEN3)
Text Label 1100 1500 0    50   ~ 0
RP_GPIO27(GEN2)
Text Label 1100 1400 0    50   ~ 0
RP_GPIO17(GEN0)
Text Label 1100 1200 0    50   ~ 0
RP_GPIO4(GCLK)
Text Label 1100 1100 0    50   ~ 0
RP_GPIO3(SCL1)
$Comp
L HegerBoard_Kicadlib:40Pin_DE10Nano_Markju FPGA1
U 1 1 5E997E64
P 9300 4800
F 0 "FPGA1" H 9300 6615 50  0000 C CNN
F 1 "40Pin_DE10Nano_Markju" H 9300 6524 50  0000 C CNN
F 2 "HegerBoard2_1KiCadLib:40Pin_DE10Nano_markju" H 8350 6050 50  0001 C CNN
F 3 "" H 8350 6050 50  0001 C CNN
	1    9300 4800
	1    0    0    -1  
$EndComp
Text Label 1100 1000 0    50   ~ 0
GPIO2(SDA1)
Wire Wire Line
	5350 2150 5050 2150
Text GLabel 5050 2150 0    50   Input ~ 0
LaserPil
Text GLabel 5050 2250 0    50   Input ~ 0
LaserPuls
Text GLabel 5050 2350 0    50   Input ~ 0
Safe_Sig
Text GLabel 5050 2450 0    50   Input ~ 0
Fail_Out
Text GLabel 5050 2550 0    50   Input ~ 0
USV_Sig
Text GLabel 5050 2650 0    50   Input ~ 0
ATX_PW_OK
Text GLabel 5050 2750 0    50   Input ~ 0
FPGA_Relay3_Safety
Text GLabel 5050 2850 0    50   Input ~ 0
FPGA_Relay2_Air
Text GLabel 5050 2950 0    50   Input ~ 0
FPGA_Relay1_Light
Text GLabel 5050 3050 0    50   Input ~ 0
LVDS_CLK
Text GLabel 5050 3150 0    50   Input ~ 0
LVDS_Sync
Text GLabel 5050 3250 0    50   Input ~ 0
LVDS_x
Text GLabel 5050 3350 0    50   Input ~ 0
LVDS_y
Wire Wire Line
	5350 2250 5050 2250
Wire Wire Line
	5050 2350 5350 2350
Wire Wire Line
	5350 2450 5050 2450
Wire Wire Line
	5050 2550 5350 2550
Wire Wire Line
	5350 2650 5050 2650
Wire Wire Line
	5050 2750 5350 2750
Wire Wire Line
	5350 2850 5050 2850
Wire Wire Line
	5050 2950 5350 2950
Wire Wire Line
	5350 3050 5050 3050
Wire Wire Line
	5050 3150 5350 3150
Wire Wire Line
	5350 3250 5050 3250
Wire Wire Line
	5050 3350 5350 3350
Text GLabel 7650 3350 2    50   Input ~ 0
FPGA_GPIO[34]
Text GLabel 7650 3250 2    50   Input ~ 0
FPGA_GPIO[32]
Text GLabel 7650 3150 2    50   Input ~ 0
FPGA_GPIO[30]
Text GLabel 7650 3050 2    50   Input ~ 0
FPGA_GPIO[28]
Text GLabel 7650 2950 2    50   Input ~ 0
FPGA_GPIO[25]
Wire Wire Line
	7650 2850 7400 2850
Wire Wire Line
	7400 2750 7650 2750
Wire Wire Line
	7650 2650 7400 2650
Wire Wire Line
	7400 2550 7650 2550
Wire Wire Line
	7650 2950 7400 2950
Wire Wire Line
	7400 3050 7650 3050
Wire Wire Line
	7650 3150 7400 3150
Wire Wire Line
	7400 3250 7650 3250
Wire Wire Line
	7650 3350 7400 3350
Text GLabel 7650 2850 2    50   Input ~ 0
RP_SPI_CE
Text GLabel 7650 2550 2    50   Input ~ 0
RP_SPI_MOSI
Text GLabel 7650 2650 2    50   Input ~ 0
RP_SPI_MISO
Text GLabel 7650 2750 2    50   Input ~ 0
RP_SPI_SCLK
Text GLabel 3800 2000 2    50   Input ~ 0
RP_SPI_CE
Text GLabel 1100 2000 0    50   Input ~ 0
RP_SPI_SCLK
Text GLabel 1100 1900 0    50   Input ~ 0
RP_SPI_MISO
Text GLabel 1100 1800 0    50   Input ~ 0
RP_SPI_MOSI
Text GLabel 8350 6150 0    50   Input ~ 0
FPGA_GPIO[34]
Text GLabel 8350 6000 0    50   Input ~ 0
FPGA_GPIO[32]
Text GLabel 8350 5850 0    50   Input ~ 0
FPGA_GPIO[30]
Text GLabel 8350 5700 0    50   Input ~ 0
FPGA_GPIO[28]
Text GLabel 10150 5250 2    50   Input ~ 0
FPGA_GPIO[25]
Wire Wire Line
	8350 5700 8550 5700
Wire Wire Line
	8350 6150 8550 6150
Wire Wire Line
	8550 6000 8350 6000
Wire Wire Line
	8350 5850 8550 5850
Wire Wire Line
	10150 5250 10050 5250
Text GLabel 10150 3450 2    50   Input ~ 0
LaserPil
Text GLabel 10150 4350 2    50   Input ~ 0
LaserPuls
Text GLabel 8350 4800 0    50   Input ~ 0
Safe_Sig
Text GLabel 8350 4950 0    50   Input ~ 0
Fail_Out
Text GLabel 8350 5100 0    50   Input ~ 0
USV_Sig
Text GLabel 8350 5250 0    50   Input ~ 0
ATX_PW_OK
Text GLabel 10150 4650 2    50   Input ~ 0
FPGA_Relay3_Safety
Text GLabel 10150 4800 2    50   Input ~ 0
FPGA_Relay2_Air
Text GLabel 10150 4950 2    50   Input ~ 0
FPGA_Relay1_Light
Text GLabel 10150 5550 2    50   Input ~ 0
LVDS_CLK
Text GLabel 10150 5700 2    50   Input ~ 0
LVDS_Sync
Text GLabel 10150 5850 2    50   Input ~ 0
LVDS_x
Text GLabel 10150 6000 2    50   Input ~ 0
LVDS_y
Wire Wire Line
	10150 6000 10050 6000
Wire Wire Line
	10150 5850 10050 5850
Wire Wire Line
	10150 5700 10050 5700
Wire Wire Line
	10150 5550 10050 5550
Wire Wire Line
	10150 3450 10050 3450
Wire Wire Line
	10150 4350 10050 4350
Wire Wire Line
	10050 4650 10150 4650
Wire Wire Line
	10150 4950 10050 4950
Wire Wire Line
	10150 4800 10050 4800
Wire Wire Line
	8350 5100 8550 5100
Wire Wire Line
	8350 5250 8550 5250
Wire Wire Line
	8350 4950 8550 4950
Wire Wire Line
	8350 4800 8550 4800
$Comp
L power:GND #PWR0101
U 1 1 5EAA5F92
P 10100 6300
F 0 "#PWR0101" H 10100 6050 50  0001 C CNN
F 1 "GND" H 10105 6127 50  0000 C CNN
F 2 "" H 10100 6300 50  0001 C CNN
F 3 "" H 10100 6300 50  0001 C CNN
	1    10100 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	10100 6300 10100 5400
Wire Wire Line
	10100 4050 10050 4050
Wire Wire Line
	10050 5400 10100 5400
Connection ~ 10100 5400
Wire Wire Line
	10100 5400 10100 4050
$Comp
L power:GND #PWR0102
U 1 1 5EAAEF8C
P 7500 4450
F 0 "#PWR0102" H 7500 4200 50  0001 C CNN
F 1 "GND" H 7505 4277 50  0000 C CNN
F 2 "" H 7500 4450 50  0001 C CNN
F 3 "" H 7500 4450 50  0001 C CNN
	1    7500 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 2250 7400 2250
$Comp
L power:GND #PWR0103
U 1 1 5EAB7226
P 6950 4450
F 0 "#PWR0103" H 6950 4200 50  0001 C CNN
F 1 "GND" H 6955 4277 50  0000 C CNN
F 2 "" H 6950 4450 50  0001 C CNN
F 3 "" H 6950 4450 50  0001 C CNN
	1    6950 4450
	1    0    0    -1  
$EndComp
$Comp
L HegerBoard_Kicadlib:cyc1000 U1
U 1 1 5E99B5B5
P 5450 2350
F 0 "U1" H 5500 3150 50  0000 C CNN
F 1 "cyc1000" H 6200 100 50  0000 C CNN
F 2 "HegerBoard2_1KiCadLib:cyc1000" H 5450 2350 50  0001 C CNN
F 3 "" H 5450 2350 50  0001 C CNN
	1    5450 2350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5EAC1DFA
P 5800 4450
F 0 "#PWR0104" H 5800 4200 50  0001 C CNN
F 1 "GND" H 5805 4277 50  0000 C CNN
F 2 "" H 5800 4450 50  0001 C CNN
F 3 "" H 5800 4450 50  0001 C CNN
	1    5800 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5EAC5C51
P 7500 1350
F 0 "#PWR0105" H 7500 1100 50  0001 C CNN
F 1 "GND" H 7505 1177 50  0000 C CNN
F 2 "" H 7500 1350 50  0001 C CNN
F 3 "" H 7500 1350 50  0001 C CNN
	1    7500 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 1350 7500 1300
Wire Wire Line
	7500 1300 6350 1300
Wire Wire Line
	6350 1300 6350 1500
Text GLabel 1100 2700 0    50   Input ~ 0
USV_Sig
Wire Wire Line
	7750 1050 7750 2050
Wire Wire Line
	7750 2050 7400 2050
Wire Wire Line
	7400 1800 7500 1800
Wire Wire Line
	7500 1800 7500 2250
Connection ~ 7500 2250
Wire Wire Line
	7500 2250 7500 3550
Wire Wire Line
	7400 3550 7500 3550
Connection ~ 7500 3550
Wire Wire Line
	7500 3550 7500 4450
$Comp
L power:GND #PWR0107
U 1 1 5EAFF5B1
P 5250 4450
F 0 "#PWR0107" H 5250 4200 50  0001 C CNN
F 1 "GND" H 5255 4277 50  0000 C CNN
F 2 "" H 5250 4450 50  0001 C CNN
F 3 "" H 5250 4450 50  0001 C CNN
	1    5250 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 4450 5250 3550
Wire Wire Line
	5250 3550 5350 3550
Wire Wire Line
	5250 3550 5250 1800
Wire Wire Line
	5250 1800 5350 1800
Connection ~ 5250 3550
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5EB0AE1A
P 850 5900
F 0 "H1" V 1087 5903 50  0000 C CNN
F 1 "MountingHole_Pad" V 996 5903 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 850 5900 50  0001 C CNN
F 3 "~" H 850 5900 50  0001 C CNN
	1    850  5900
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5EB0BD69
P 850 6250
F 0 "H2" V 1087 6253 50  0000 C CNN
F 1 "MountingHole_Pad" V 996 6253 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 850 6250 50  0001 C CNN
F 3 "~" H 850 6250 50  0001 C CNN
	1    850  6250
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5EB22E1F
P 850 6600
F 0 "H3" V 1087 6603 50  0000 C CNN
F 1 "MountingHole_Pad" V 996 6603 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 850 6600 50  0001 C CNN
F 3 "~" H 850 6600 50  0001 C CNN
	1    850  6600
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5EB231B8
P 850 6950
F 0 "H4" V 1087 6953 50  0000 C CNN
F 1 "MountingHole_Pad" V 996 6953 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 850 6950 50  0001 C CNN
F 3 "~" H 850 6950 50  0001 C CNN
	1    850  6950
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5EB2B0CA
P 1050 7250
F 0 "#PWR0108" H 1050 7000 50  0001 C CNN
F 1 "GND" H 1055 7077 50  0000 C CNN
F 2 "" H 1050 7250 50  0001 C CNN
F 3 "" H 1050 7250 50  0001 C CNN
	1    1050 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 7250 1050 6950
Wire Wire Line
	1050 5900 950  5900
Wire Wire Line
	950  6250 1050 6250
Connection ~ 1050 6250
Wire Wire Line
	1050 6250 1050 5900
Wire Wire Line
	950  6600 1050 6600
Connection ~ 1050 6600
Wire Wire Line
	1050 6600 1050 6250
Wire Wire Line
	950  6950 1050 6950
Connection ~ 1050 6950
Wire Wire Line
	1050 6950 1050 6600
Wire Wire Line
	6950 4450 6950 3950
Wire Wire Line
	6950 3950 6800 3950
Wire Wire Line
	5950 3950 5800 3950
Wire Wire Line
	5800 3950 5800 4450
Wire Wire Line
	6100 1500 6100 1300
Wire Wire Line
	6100 1300 6350 1300
Connection ~ 6350 1300
$Comp
L power:+5V #PWR0106
U 1 1 5EA948E0
P 7750 1050
F 0 "#PWR0106" H 7750 900 50  0001 C CNN
F 1 "+5V" H 7750 1190 50  0000 C CNN
F 2 "" H 7750 1050 50  0000 C CNN
F 3 "" H 7750 1050 50  0000 C CNN
	1    7750 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 1700 2250 1700
Wire Wire Line
	1100 900  2250 900 
Text Label 1100 900  0    50   ~ 0
+3V3VCC1
Text Label 1100 1700 0    50   ~ 0
+3V3VCC2
$EndSCHEMATC
