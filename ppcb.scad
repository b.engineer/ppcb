include <BOSL/constants.scad>
use <BOSL/transforms.scad>


difference(){
hull(){
	linear_extrude(height = 3, center = false) import("Edge_Cuts.svg");
}
up(2.5)linear_extrude(height = 0.5, center = false) import("F_Cu.svg");
linear_extrude(height = 4, center = false) import("B_Mask.svg");
linear_extrude(height = 0.5, center = false) import("B_Cu.svg");
}
