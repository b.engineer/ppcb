include <BOSL/constants.scad>
use <BOSL/transforms.scad>
use <BOSL/shapes.scad>

$fn = 20;
x = 100;
y = 100;
z = 1.5;
eps = 0.01;

hs = 1.6; // hole size
p = 2.54; // pitch
m = 7; // margin
mh = 4; // mounting holes

difference(){
cuboid([x,y,z]);
grid2d(size=[x - 2*m,y - 2*m], spacing=p, stagger=false) cuboid([hs,hs,z+eps]);
//grid2d(size=[x - 2*m,y - 2*m], spacing=p, stagger=false) cyl(l=z+eps,d=hs);
grid2d(size=[x,y], spacing=[x-1.7*mh,y-1.7*mh], stagger=false) cyl(l=z+eps,d=mh);
}
